# Mazes

This is the implementation I've made following the book [Mazes for Programmers](https://pragprog.com/book/jbmaze/mazes-for-programmers).

I use kotlin instead of ruby, so details may be different, but main logic is the same.
