package me.d2349.experiments.mazes.visualizer

import me.d2349.experiments.mazes.common.Board
import me.d2349.experiments.mazes.common.Grid

private const val horizontal = "--+"
private const val vertical = "  |"
private const val openHorizontal = "   "
private const val openVertical = "  +"


fun mazeAsString(grid: Grid): String {
    return genericMazeAsString(
        grid,
        { b, row, col -> eastWallGrid(b, row - 1, col - 1) },
        { b, row, col -> southWallGrid(b, row - 1, col - 1) })

}

private fun eastWallGrid(grid: Grid, row: Int, col: Int): Boolean {
    val cell = grid.grid[row][col]

    return cell.links.contains(cell.east)
}

private fun southWallGrid(grid: Grid, row: Int, col: Int): Boolean {
    val cell = grid.grid[row][col]
    return cell.links.contains(cell.south)
}

private fun <T : Board> genericMazeAsString(
    board: T,
    eastWallQ: (T, Int, Int) -> Boolean,
    southWallQ: (T, Int, Int) -> Boolean
): String {
    val maze = StringBuilder("")
    val topWall = topWall(board.width)

    maze.append(topWall).append("\n")

    for (row in board.height downTo 1) {
        val rowMiddle = StringBuilder("|")
        val rowBottom = StringBuilder("+")

        for (col in 1..board.width) {

            val eastWall = if (eastWallQ(board, row, col)) {
                openHorizontal
            } else {
                vertical
            }

            val southWall = if (southWallQ(board, row, col)) {
                openVertical
            } else {
                horizontal
            }

            rowMiddle.append(eastWall)
            rowBottom.append(southWall)
        }

        maze.append(rowMiddle).append("\n")
            .append(rowBottom).append("\n")
    }


    return maze.toString()
}


private fun topWall(width: Int): StringBuilder {
    val topWall = StringBuilder("+")
    repeat(width) {
        topWall.append(horizontal)
    }
    return topWall
}
