package me.d2349.experiments.mazes.common

enum class Direction {
    EAST, WEST, NORTH, SOUTH
}

class LinkedCell(val row: Int, val col: Int) {
    var links: List<LinkedCell> = listOf()
    var north: LinkedCell? = null
    var south: LinkedCell? = null
    var east: LinkedCell? = null
    var west: LinkedCell? = null

    fun link(cell: LinkedCell, linkBack: Boolean = true): LinkedCell {
        links = links + cell

        if (linkBack)
            cell.link(this, false)

        return this
    }

    fun unlink(cell: LinkedCell, unlinkBack: Boolean = true): LinkedCell {
        links = links - cell

        if (unlinkBack)
            cell.unlink(this, false)

        return this
    }
}

abstract class Board(val width: Int, val height: Int)

class Grid(width: Int, height: Int) : Board(width, height) {
    val grid: List<List<LinkedCell>> = prepareGrid()

    init {
        configureCells()
    }

    fun openLink(row: Int, col: Int, direction: Direction): Grid {
        val cell = grid[row - 1][col - 1]
        return cellOperation(cell, direction) { c -> c?.link(cell) }
    }

    fun closeLink(row: Int, col: Int, direction: Direction): Grid {
        val cell = grid[row - 1][col - 1]
        return cellOperation(cell, direction) { c -> c?.unlink(cell) }
    }

    private fun cellOperation(
        mainCell: LinkedCell,
        direction: Direction,
        operation: (LinkedCell?) -> Unit
    ): Grid {

        val otherCell = when (direction) {
            Direction.EAST -> mainCell.east
            Direction.WEST -> mainCell.west
            Direction.NORTH -> mainCell.north
            Direction.SOUTH -> mainCell.south
        }

        operation(otherCell)

        return this
    }


    private fun configureCells() {
        for (row in 0 until height)
            for (col in 0 until width) {
                val westCell: LinkedCell? = if (col > 0) {
                    grid[row][col - 1]
                } else {
                    null
                }

                val eastCell: LinkedCell? = if (col < width - 1) {
                    grid[row][col + 1]
                } else {
                    null
                }

                val northCell: LinkedCell? = if (row < height - 1) {
                    grid[row + 1][col]
                } else {
                    null
                }

                val southCell: LinkedCell? = if (row > 0) {
                    grid[row - 1][col]
                } else {
                    null
                }

                val cell = grid[row][col]
                cell.north = northCell
                cell.south = southCell
                cell.east = eastCell
                cell.west = westCell

            }
    }

    private fun prepareGrid(): List<List<LinkedCell>> {
        val grid: MutableList<MutableList<LinkedCell>> = mutableListOf()

        for (row in 0 until height) {
            val rowList = mutableListOf<LinkedCell>()
            for (col in 0 until width) {
                rowList.add(LinkedCell(row, col))
            }
            grid.add(rowList)
        }

        return grid
    }

}
