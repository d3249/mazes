package me.d2349.experiments.mazes.generator

import me.d2349.experiments.mazes.common.Grid
import me.d2349.experiments.mazes.common.LinkedCell
import kotlin.random.Random

fun binaryTree(width: Int, height: Int): Grid {
    val grid = Grid(width, height)

    for (row in 0 until width)
        for (col in 0 until height) {

            val eastLink = Random.nextBoolean()
            val cell = grid.grid[row][col]

            when {
                row == (height - 1) && col == (width - 1) -> null
                row == (height - 1) -> cell.east?.link(cell)
                col == (width - 1) -> cell.north?.link(cell)
                else -> if (eastLink) {
                    cell.east?.link(cell)
                } else {
                    cell.north?.link(cell)
                }
            }

        }

    return grid
}

fun sidewinder(width: Int, height: Int): Grid {
    val grid = Grid(width, height)

    for (row in 0 until height) {
        val run = mutableListOf<LinkedCell>()
        for (col in 0 until width) {

            val eastLink = Random.nextBoolean()
            val cell = grid.grid[row][col]

            when {
                row == (height - 1) && col == (width - 1) -> null
                row == (height - 1) -> {
                    run.add(cell)
                    cell.east?.link(cell)
                }
                col == (width - 1) -> {
                    run.add(cell)
                    closeRun(run)
                    run.removeAll { true }
                }
                else -> if (eastLink) {
                    run.add(cell)
                    cell.east?.link(cell)
                } else {
                    run.add(cell)
                    closeRun(run)
                    run.removeAll { true }
                }
            }


        }
    }

    return grid
}

private fun closeRun(run: List<LinkedCell>) {
    val exit = run.random()
    exit.north?.link(exit)
}
