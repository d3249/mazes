import me.d2349.experiments.mazes.generator.binaryTree
import me.d2349.experiments.mazes.generator.sidewinder
import me.d2349.experiments.mazes.visualizer.mazeAsString


var board1 = binaryTree(10, 10)
print("\n=== Binary Tree ===\n")
println(mazeAsString(board1))

var board2 = sidewinder(10, 10)
print("\n=== Sidewinder ===\n")
println(mazeAsString(board2))
