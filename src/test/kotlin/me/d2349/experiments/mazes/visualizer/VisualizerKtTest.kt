package me.d2349.experiments.mazes.visualizer

import io.kotlintest.specs.StringSpec
import io.kotlintest.shouldBe
import me.d2349.experiments.mazes.common.Direction
import me.d2349.experiments.mazes.common.Grid

class VisualizerKtTest : StringSpec() {

    init {
        "It draws a single square board" {
            val expected = """+--+
                             >|  |
                             >+--+
                             >""".trimMargin(">")
            val board = Grid(1, 1)

            mazeAsString(board) shouldBe expected
        }

        "It draws a single row board" {

            val expected = """+--+--+--+
                             >|  |  |  |
                             >+--+--+--+
                             >""".trimMargin(">")
            val board = Grid(3, 1)

            mazeAsString(board) shouldBe expected
        }

        "It draws a single row with a horizontal passage"{

            val expected = """+--+--+--+
                             >|        |
                             >+--+--+--+
                             >""".trimMargin(">")
            val board = Grid(3, 1)
                .openLink(1, 1, Direction.EAST)
                .openLink(1, 2, Direction.EAST)

            mazeAsString(board) shouldBe expected
        }

        "It draws a single row with a segmented passage"{

            val expected = """+--+--+--+--+
                             >|     |  |  |
                             >+--+--+--+--+
                             >""".trimMargin(">")
            val board = Grid(4, 1)
                .openLink(1, 1, Direction.EAST)

            mazeAsString(board) shouldBe expected
        }

        "It draws a 2x2 unconnected maze"{

            val expected = """+--+--+
                             >|  |  |
                             >+--+--+
                             >|  |  |
                             >+--+--+
                             >""".trimMargin(">")

            val board = Grid(2, 2)

            mazeAsString(board) shouldBe expected
        }

        "It draws a single column" {
            val expected = """+--+
                             >|  |
                             >+--+
                             >|  |
                             >+--+
                             >|  |
                             >+--+
                             >""".trimMargin(">")
            val board = Grid(1, 3)

            mazeAsString(board) shouldBe expected
        }

        "It draws a single column with a passage" {
            val expected = """+--+
                             >|  |
                             >+  +
                             >|  |
                             >+  +
                             >|  |
                             >+--+
                             >""".trimMargin(">")
            val board = Grid(1, 3)
                .openLink(1, 1, Direction.NORTH)
                .openLink(2, 1, Direction.NORTH)

            mazeAsString(board) shouldBe expected
        }

        "It draws a single column with a passage segmented" {
            val expected = """+--+
                             >|  |
                             >+--+
                             >|  |
                             >+  +
                             >|  |
                             >+--+
                             >""".trimMargin(">")
            val board = Grid(1, 3)
                .openLink(1, 1, Direction.NORTH)

            mazeAsString(board) shouldBe expected
        }

        "It draws a custom maze"{

            val expected = """+--+--+--+
                             >|        |
                             >+  +  +--+
                             >|  |     |
                             >+--+  +  +
                             >|     |  |
                             >+--+--+--+
                             >""".trimMargin(">")
            val board = Grid(3, 3)
                .openLink(1, 1, Direction.EAST)
                .openLink(1, 2, Direction.NORTH)
                .openLink(1, 3, Direction.NORTH)
                .openLink(2, 1, Direction.NORTH)
                .openLink(2, 2, Direction.NORTH)
                .openLink(2, 2, Direction.EAST)
                .openLink(3, 1, Direction.EAST)
                .openLink(3, 2, Direction.EAST)

            mazeAsString(board) shouldBe expected
        }
    }

}