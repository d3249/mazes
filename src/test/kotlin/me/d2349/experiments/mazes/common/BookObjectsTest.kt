package me.d2349.experiments.mazes.common

import io.kotlintest.specs.StringSpec
import io.kotlintest.shouldBe

class BookObjectsTest : StringSpec() {

    init {
        "We can create a single cell with no neighbors"{
            val cell = LinkedCell(1, 1)

            cell.north shouldBe null
            cell.south shouldBe null
            cell.east shouldBe null
            cell.west shouldBe null
        }

        "We can add a neighbor" {
            val cell1 = LinkedCell(1, 1)
            val cell2 = LinkedCell(1, 2)

            cell1.east = cell2
            cell2.west = cell1

            cell1.east shouldBe cell2
            cell2.west shouldBe cell1

        }

        "We can remove a neighbor"{

            val cell1 = LinkedCell(1, 1)
            val cell2 = LinkedCell(1, 2)

            cell1.east = cell2
            cell2.west = cell1

            cell1.east shouldBe cell2
            cell2.west shouldBe cell1

            cell1.east = null
            cell2.west = null

            cell1.east shouldBe null
            cell2.west shouldBe null
        }

        "We can create a grid" {
            val grid = Grid(2, 3)

            grid.width shouldBe 2
            grid.height shouldBe 3
        }
    }

}