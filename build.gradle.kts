plugins {
    java
    kotlin("jvm") version "1.3.61"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    testImplementation("junit", "junit", "4.12")
    testImplementation("io.kotlintest", "kotlintest-runner-junit5", "3.4.2")
    testImplementation("io.mockk", "mockk", "1.9.3")
    implementation(kotlin("script-runtime"))
}

val test by tasks.getting(Test::class){
    useJUnitPlatform {}
    testLogging.showStandardStreams = true
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}
tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "11"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "11"
    }
}